
# 2020-dynamic-analysis-android

Bachelor's thesis Leon Weiß

## Abstract

As mobile devices become more and more ubiquitous in our daily life, they also increasingly penetrate our private spaces. With more than 90% of the world's population using at least one device, we are constantly surrounded by them. 

Looking at the sensitive data our phones and other devices hold, the question arises whether or not they are trustworthy enough to store our secrets. Therefore assessing the privacy impact of mobile devices and applications is an important field of study. 

This thesis aims at providing a tool facilitating future research on the topic of cross-platform privacy assessments. To achieve this goal, the work is based on a theoretical background and a well developed app model, which are then applied to form a concrete implementation using state-of-the-art tools. This implementation is verified and improved through experiments with various applications and setups.

The new tool proposed by this thesis is capable of fully automating the dynamic analysis of Android apps. The tool takes an APK, installs it onto a virtual or physical Android phones and explores the application's GUI. It captures all network traffic generated during analysis and stores it in a PCAPNG file for further analysis.

Evaluation on a diverse set of Android apps taken from different categories in the Google Play Store shows that the tool proposed by this thesis works well with current versions of different applications on even the latest Android version.
