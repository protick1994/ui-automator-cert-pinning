import logging
import time
from typing import TYPE_CHECKING

from appium import webdriver
from selenium.common.exceptions import WebDriverException
from urllib3.exceptions import MaxRetryError

from Android.app import AndroidApp
from Android.device import AndroidDevice
from Android.ui_automator.app_model import AndroidAppModel
from Android.ui_automator.constants import ANDROID_HOME_KEY_CODE
from common.exploration_session import ExplorationSession
from helper.constants import LOGGER_BASE_NAME

if TYPE_CHECKING:
    from typing import Type

    from Android.ui_automator.exploration_strategies import \
        AndroidExplorationStrategy
    from common.exploration_benchmarks import ExplorationBenchmark
    from helper.storage_helper import StorageHelper

logger = logging.getLogger(LOGGER_BASE_NAME + ".exploration-sessions")


class AndroidExplorationSession(ExplorationSession):
    """Holds all data necessary for exploration, manages Appium session."""
    app_model: "AndroidAppModel"
    explorer: "AndroidExplorationStrategy"
    appium_wd: "webdriver.Remote"

    def __init__(
        self,
        command_executor: str,
        device: AndroidDevice,
        app: AndroidApp,
        install_app_from_apk: "bool" = True,
    ):
        """Initializes session with given data.

        Args:
            command_executor: Address of the Appium instance.
            device: Android device the app is to be run on.
            app: Information about the app to be explored
            install_app_from_apk: If True, the app is installed from APK before analysis.
                                 Else it is assumed that the app is already installed. Defaults to True.
        """
        self.command_executor = command_executor
        self.device = device
        self.app = app
        self.install_app = install_app_from_apk

    def start(self) -> None:
        """Starts an Appium-session using the device and app provided during initialization. Initializes AppModel.

        Raises:
            MaxRetryError: If Appium does not respond.
            WebDriverException: If an unexpected error is encountered by Appium during session start.
        """
        if not hasattr(self, "appium_wd") or not self.appium_wd:
            start_activity = None
            # If path to APK is specified, Appium collects necessary information on its own
            if not self.install_app:
                # If Start-Activity is not already specified and there are main activities available,
                # use one whose name starts with the package name
                if not self.app.launch_activity and self.app.main_activities:
                    start_activities = [
                        activity
                        for activity in self.app.main_activities
                        if activity.startswith(self.app.app_package)
                    ]
                    if start_activities:
                        start_activity = start_activities[0]
                    else:
                        # Catch rare edge case where no main activity name is starting with package_name
                        start_activity = [
                            activity for activity in self.app.main_activities
                        ][0]
                else:
                    start_activity = self.app.launch_activity

            desired_caps = {
                "platformName": "Android",
                "automationName": "UiAutomator2",
                "platformVersion": self.device.platform_version,
                "app": (str(self.app.apk_path) if self.install_app else None),
                "deviceName": self.device.name,
                "udid": self.device.udid,
                "newCommandTimeout": 600,
                "avd": self.device.avd_name,
                "appPackage": self.app.app_package,
                "appActivity": start_activity,
                # If app.app_activity is not explicitly set, allow all activities of the package in order to handle
                # init activities different from the main activity specified in AndroidManifest.xml
                "appWaitActivity": (
                    "*" if not self.app.launch_activity else self.app.launch_activity
                ),
                "autoGrantPermissions": True,
                # Reset and uninstall app before running tests
                "fullReset": self.install_app,
            }
            logger.info("Starting session")

            try:
                self.appium_wd = webdriver.Remote(self.command_executor, desired_caps)
            except MaxRetryError as err:
                logger.critical("Could not connect to WDA command executor")
                raise err

            self.app_model = AndroidAppModel(session=self)

    def stop(self) -> None:
        """Quits the running Appium session. Attempts to close app and return to home screen if using physical device or
        emulator is still running.
        """
        if hasattr(self, "appium_wd") and self.appium_wd:
            try:
                if not self.device.emulator or (
                    self.device.emulator and self.device.emulator.is_running()
                ):
                    logger.debug("Returning to device home screen")
                    self.appium_wd.close_app()
                    self.appium_wd.press_keycode(ANDROID_HOME_KEY_CODE)
                    # TODO: Close all user opened apps

                logger.info("Quitting Appium session")
                self.appium_wd.quit()
            except WebDriverException:
                logger.warning(
                    "Stopping session did not go gracefully: Appium raised WebDriverException", exc_info=True
                )

    def explore(
        self,
        explorer: "Type[AndroidExplorationStrategy]",
        benchmark: "Type[ExplorationBenchmark]",
        results_helper: "StorageHelper.AnalysisResultsHelper",
        steps: int,
    ) -> None:
        """Explores the app automatically using the given Strategy, Benchmark and number of steps. Stores results with
        the help of StorageHelper. Starts the session if Appium is not running yet.

        Args:
            explorer: ExplorationStrategy handling actual app analysis and control.
            benchmark: ExplorationBenchmark analysing and measuring explorer behaviour.
            results_helper: AnalysisResultsHelper used to store exploration results and steps to disk.
            steps: Number of steps for which app should be explored.

        Raises:
            WebDriverException: If an unexpected error is encountered by Appium during exploration. Results are
                                stored nevertheless.
        """
        logger.info("initial steps {}".format(steps))
        if not hasattr(self, "appium_wd"):
            logger.info("No Appium session found, starting new one.")
            self.start()

        # This should include the app itself
        processes_before_analysis = set(
            self.appium_wd.execute_script(
                "mobile: shell", {"command": "ps", "args": ["-A", "-o USER,CMDLINE"]}
            ).splitlines()
        )
        self.explorer = explorer(self)
        self.benchmark = benchmark(self)
        start = time.time()
        try:
            self.explorer.explore(results_helper=results_helper, steps=steps)
        except WebDriverException as err:
            # Re-raise error after storing results
            raise err
        finally:
            results_helper.store_analysis_stats(app=self.app, duration=time.time()-start)

        processes_after_analysis = set(
            self.appium_wd.execute_script(
                "mobile: shell", {"command": "ps", "args": ["-A", "-o USER,CMDLINE"]}
            ).splitlines()
        )

        processes_started_during_analysis = (
            processes_after_analysis - processes_before_analysis
        )
        processes_started_during_analysis_str = "\n    ".join(
            [
                proc
                for proc in processes_started_during_analysis
                if not proc.startswith("root")
            ]
        )
        if processes_started_during_analysis_str:
            logger.info(
                f"The following non-root processes were started during the analysis: "
                f"\n    {processes_started_during_analysis_str}"
            )
