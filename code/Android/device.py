import subprocess
import tempfile
import time
from logging import getLogger
from pathlib import Path
from typing import TYPE_CHECKING

from helper.constants import LOGGER_BASE_NAME, PATH_TO_ADB, TMP_ROOT

if TYPE_CHECKING:
    from typing import List, Optional, Tuple

    from Android.emulator import AndroidEmulator

logger = getLogger(LOGGER_BASE_NAME + ".android.device")


class AndroidDevice:
    """Encapsulates information about an Android-based test device. The device may be physical or emulated."""

    name: "str"  # Emulator name - ignored by Appium
    avd_name: "str"  # Name of Android Virtual device to launch
    udid: "str"  # Must be specified as name is ignored for Android (seen when running `adb devices`)
    backup_path: "Optional[Path]"
    platform_version: "str"
    platform_name: "str" = "Android"
    emulator: "Optional[AndroidEmulator]" = None

    def __init__(
        self,
        name: "str",
        avd_name: "str",
        udid: "str",
        backup_path: "Optional[Path]",
        platform_version: "str",
        emulator: "Optional[AndroidEmulator]" = None,
    ) -> None:
        """Initializes AndroidDevice object.

            Args:
                name: Name of the device. Currently unused.
                avd_name: Name of the AVD, empty string if unknown.
                udid: UDID of the device as shown by ``adb devices``. Used to connect to device via Appium and adb.
                backup_path: Path to zip file containing backup of AVD.
                platform_version: Android version (e.g. 4.2 or 11)
                emulator: Emulator object if device is AVD.
        """
        self.name = name
        self.avd_name = avd_name
        self.udid = udid
        self.backup_path = backup_path
        self.platform_version = platform_version
        self.emulator = emulator

    def issue_adb_command(
        self, arguments: "List[str]", timeout: "int" = 10
    ) -> "Tuple[Optional[int], str]":
        """Sends adb command to emulator and waits for response, takes care that correct emulator is reached.
        If subprocess is not finished after timeout seconds, it is killed and (None, "") is returned.

        Args:
            arguments: List of arguments sent using adb. Do not include -s.
            timeout: Number of seconds allowed for the command to take before it is terminated.
                        Negative numbers lead to unexpected behavior.
        Returns:
            Tuple of returncode and output to stdout
        """
        returncode = None
        output = ""

        command_line = [
            PATH_TO_ADB,
            "-s",
            self.udid,
        ] + arguments

        start = time.time()
        with subprocess.Popen(
            command_line,
            stdout=subprocess.PIPE,
            text=True,
        ) as proc:
            while returncode is None and time.time() <= start + timeout:
                returncode = proc.poll()
            if returncode is None:
                proc.kill()
            else:
                output = proc.stdout.read()
        return returncode, output

    def extract_apk(self, pkg_name: "str") -> "Optional[Path]":
        _logger = getLogger(logger.name + ".extract_apk")
        file_handle, file_path = tempfile.mkstemp(dir=TMP_ROOT)
        return_code, output = self.issue_adb_command(
            [
                "shell",
                "pm",
                "path " + pkg_name + " | awk -F':' '{print $2}'",
            ],
        )

        # TODO: Develop better solution
        # this solution discards al auxiliary apks (like split configs)
        paths = [path for path in output.splitlines(keepends=False) if path.endswith("base.apk")]

        if len(paths) > 1:
            _logger.error(f"More than one path available. ADB-Output: {output}")
            # TODO: Evaluate whether it makes sense to simply use first entry
            return None

        path_on_device = paths[0]

        returncode, output = self.issue_adb_command(
            ["pull", path_on_device, file_path]
        )
        if return_code != 0:
            _logger.error(f"ADB returned {return_code} ADB-Output: {output}")
            return None
        return Path(file_path)
