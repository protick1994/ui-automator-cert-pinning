from dataclasses import dataclass
from hashlib import sha256
from logging import getLogger
from pathlib import Path
from typing import TYPE_CHECKING

import androguard
from androguard.core.bytecodes.apk import APK

from helper.constants import LOGGER_BASE_NAME

if TYPE_CHECKING:
    from pathlib import Path
    from typing import List, Optional, Set, Union


class AndroidApp:
    """Encapsulates information about an Android-app, parsed by androguard."""

    name: "str"
    apk_path: "Path"
    app_package: "str"
    launch_activity: "Optional[str]" = None  # Is only set in special cases
    version_string: "str"
    version_code: "str"
    main_activities: "List[str]"
    activities: "List[str]"
    apk_object: "Optional[APK]"
    sha256: "str"
    install_source: "str"

    def __init__(self, path: "Union[str, Path]"):
        """Initializes object and parses APK specified by path using androguard.

        Args:
            path: Path to an APK-file

        Raises:
            FileNotFoundError: If path is not a file.
        """
        if isinstance(path, str):
            path = Path(path)
        if not path.is_file():
            raise FileNotFoundError(path)
        self.apk_path = path
        self._logger = getLogger(LOGGER_BASE_NAME + "android.app")

        with self.apk_path.open(mode="rb") as file:
            sha256hash = sha256()
            for byte_block in iter(lambda: file.read(4096), b""):
                sha256hash.update(byte_block)
            self.sha256 = sha256hash.hexdigest()

        self.apk_object = self._parse_apk(self.apk_path)
        if self.apk_object:
            self.name = self.apk_object.get_app_name()
            self.app_package = self.apk_object.get_package()
            self.activities = self.apk_object.get_activities()
            self.main_activities = self.apk_object.get_main_activities()
            self.version_string = self.apk_object.get_androidversion_name()
            self.version_code = self.apk_object.get_androidversion_code()
        elif path.name.lower().endswith(".apk"):
            self.name = self.app_package = path.name[: -len(".apk")]
            self.main_activities = self.activities = list()
            self.version_string = self.version_code = ""
            self._logger.warning(
                f"Parsing the apk failed, extrapolating app and package name from filename "
                f"({self.name})"
            )
        else:
            self.main_activities = self.activities = list()
            self.name = self.app_package = self.version_string = self.version_code = ""
            self._logger.warning(f"Parsing the apk failed, only setting path and hash")

        if self.name:
            self._logger.info(
                    f'App-Information: name="{self.name}", '
                    f'package_name="{self.app_package}", '
                    f'version="{(self.version_string if self.version_string else "UNKNOWN")}"'
                )

    @staticmethod
    def _parse_apk(path_to_apk: "Union[str, Path]") -> "Optional[APK]":
        """ Parses an APK given by path_to_apk and returns androguard result.

        Args:
            path_to_apk: Path pointing to APK-file.

        Returns:
            APK if parsing succeeded, None in other cases.

        Raises:
            FileNotFoundError: If path_to_apk is not a file.
        """
        # Suppress warnings issued by androguard
        getLogger(androguard.__name__).setLevel("ERROR")

        logger = getLogger(LOGGER_BASE_NAME + ".android.app.parse_apk")

        if isinstance(path_to_apk, str):
            path_to_apk = Path(path_to_apk)
        if not path_to_apk.is_file():
            raise FileNotFoundError(str(path_to_apk))

        logger.debug(
            f'Parsing "{str(path_to_apk)}" with androguard to obtain metadata of package.'
        )

        try:
            # Only parse manifest file to improve speed
            apk = APK(str(path_to_apk))

        except Exception:
            logger.exception(
                f'There was an exception while parsing "{str(path_to_apk)}"'
            )
            return None

        return apk

    def __eq__(self, other: "object") -> "bool":
        if isinstance(other, AndroidApp):
            return self.sha256 == other.sha256
        return False

    def __hash__(self) -> "int":
        return hash(self.sha256)


@dataclass
class AndroidActivity:
    """Holds necessary data to identify and compare Activities."""
    name: "str"
    package: "AndroidAppPackage"

    def __hash__(self) -> "int":
        return hash((self.name, self.package))

    def __str__(self) -> "str":
        return str(self.package) + self.name


@dataclass
class AndroidAppPackage:
    """Holds necessary data to identify and compare Packages."""
    name: "str"
    activities: "Set[AndroidActivity]"

    def __init__(self, name: "str"):
        self.name = name
        self.activities = set()

    def __eq__(self, other: "object") -> "bool":
        if isinstance(other, AndroidAppPackage):
            return self.name == other.name
        return False

    def __hash__(self) -> "int":
        return hash(self.name)

    def __str__(self) -> "str":
        return self.name
