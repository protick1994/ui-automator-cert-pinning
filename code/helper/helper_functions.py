from helper.seeded_random import seeded_random
import string


def get_random_string(length: "int" = 10) -> "str":
    """Returns random string of the given length made from ascii letters, digits and whitespaces

    Args:
        length: The length of the generated string. Defaults to 10. Negative values may lead to unexpected behavior.

    Returns:
        Random string
    """
    letters = string.ascii_letters + string.digits + " "
    result_str = "".join(seeded_random.choice(letters) for _ in range(length))
    return result_str
