import pyshark
import os
import pickle
import random
import sys
from multiprocessing import Process
from os import path
import json
from collections import defaultdict

pcap_paths = ["new_traffic"]

def get_files_from_path(path, extension):
    retset = set()
    for f in os.listdir(path):
        if f.endswith(extension):
            retset.add(path + "/" + f) # Need to track path as well
    return retset

pcap_files = set()
for path in pcap_paths:
    pcap_files.update(get_files_from_path(path, ".pcap"))

lst = []
for pcap in pcap_files:
    package_name = pcap[12:-5]

    lst.append(package_name)

print(lst)



