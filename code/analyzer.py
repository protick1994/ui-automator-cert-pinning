import logging

#import pyshark
from helper import seeded_random
from typing import TYPE_CHECKING

from Android.app import AndroidApp

if TYPE_CHECKING:
    from Android.device import AndroidDevice
    from pathlib import Path
    from helper.storage_helper import StorageHelper

from selenium.common.exceptions import WebDriverException

from Android.play_store import PlayStoreInstaller
from Android.ui_automator.exploration_session import AndroidExplorationSession
from Android.ui_automator.exploration_strategies import (
    AndroidIntelligentRandomButtonExplorationStrategy,
)
from common.exploration_benchmarks import StepTimerExplorationBenchmark
from helper.constants import LOGGER_BASE_NAME

import subprocess
import time
from datetime import datetime

import multiprocessing


STRATEGY = AndroidIntelligentRandomButtonExplorationStrategy
BENCHMARK = StepTimerExplorationBenchmark

STEPS = 30
MITMPROXY_ADDRESS = ""
CREATE_PCAP = False
ALLOW_REINSTALL_FROM = "NONE"

LOCAL_LOGGER_BASE_NAME = LOGGER_BASE_NAME + ".analyzer"


def analyze_app_package(
    appium_server: "str",
    android_device: "AndroidDevice",
    storage_helper: "StorageHelper",
    package_name: "str",
    version_code: "str" = "",
) -> "bool":
    """
    Installs and analyzes a given app package.

    Args:
        appium_server: Fully qualified address of the appium server
        android_device: Object holding all data of the test device
        storage_helper: Object used to help store all generated data
        package_name: Package name of app, used to find app in store
        version_code: Version code of app, not available for installations from Google Play Store
    
    Returns:
        Analysis successful
    """
    seeded_random.reset_seeded_random()

    storage_helper.results.reset()
    temp_log_handler = storage_helper.logging.add_log_file_handler()
    logger = logging.getLogger(LOCAL_LOGGER_BASE_NAME + ".analyze_app_package")

    if ALLOW_REINSTALL_FROM == "NONE":
        logger.info("Reinstalling from app store is disallowed")
        return False
    elif ALLOW_REINSTALL_FROM == "GOOGLE_PLAY":
        logger.info(f'Starting analysis of "{package_name}" after installing from Google Play Store')
        if version_code:
            # TODO: Allow to specify version code of app that is to be analyzed (requires Aurora Store)
            logger.warning("Version code is ignored, as Google Play Store does not support this")
    else:
        logger.error(f'Appstore "{ALLOW_REINSTALL_FROM}" is unknown.')
        return False

    analysis_successful = False

    try:
        if android_device.emulator:
            # Don't start from snapshot to improve speed of Google Play
            # Don't create PCAP or use MITMPROXY as installation should not be analyzed
            android_device.emulator.start(start_from_snapshot=False)
        installer = PlayStoreInstaller(android_device, appium_server)
        installed_app = installer.install_app_from_play_store(app_package=package_name)
    except Exception:
        logger.exception("An unexpected error occurred while trying to install the app")
        installed_app = None
        analysis_successful = False

    if installed_app:
        if android_device.emulator:
            if CREATE_PCAP:
                pcap_path = storage_helper.get_pcap_path(installed_app)
            else:
                pcap_path = ""

            # Don't start from backup to preserve app previously installed
            android_device.emulator.restart_without_reset(pcap_path=str(pcap_path), mitmproxy_address=MITMPROXY_ADDRESS)
        storage_helper.store_metadata_and_apk(
            installed_app,
            move_apk=True,  # Store apk in output dir to avoid it being lost at cleaning of tempdir
        )

        test_exploration_session = None
        try:
            logger.debug(f"Creating exploration session")

            test_exploration_session = AndroidExplorationSession(
                command_executor=appium_server,
                device=android_device,
                app=installed_app,
                install_app_from_apk=False,
            )
            test_exploration_session.start()
            logger.info(f'Exploring app "{installed_app.name}"')
            print("Passing step: {}".format(STEPS))
            test_exploration_session.explore(
                explorer=STRATEGY,
                benchmark=BENCHMARK,
                results_helper=storage_helper.results,
                steps=30,
            )
            logger.info(f'Finished exploring app "{installed_app.name}"')
            analysis_successful = True

        except WebDriverException:
            logger.exception(
                f'There was a "WebDriverException" while exploring the app "{installed_app.name}"'
            )

        finally:
            if test_exploration_session:
                logger.info("Stopping exploration session")
                test_exploration_session.stop()
            if android_device.emulator and android_device.emulator.is_running():
                android_device.emulator.stop()
            storage_helper.logging.close_log_file_handler(
                handler=temp_log_handler,
                android_app=installed_app,
                prefix="install-from-store-",
            )
            return analysis_successful
    else:
        if android_device.emulator:
            android_device.emulator.stop()
        logger.error(f"Installation failed")
        storage_helper.logging.close_log_file_handler(
            handler=temp_log_handler,
            android_app=None,
            prefix=f"{package_name}-failed-install-",
        )
        return False


def analyze_apk_file(
    appium_server: "str",
    android_device: "AndroidDevice",
    apk_path: "Path",
    storage_helper: "StorageHelper",
) -> "bool":
    """
    Installs and analyzes a given APK file.

    Args:
        appium_server: Fully qualified address of the appium server
        android_device: Object holding all data of the test device
        apk_path: Path pointing to the APK file
        storage_helper: StorageHelper object to help store all necessary data
    
    Returns:
        Analysis successful
    """
    seeded_random.reset_seeded_random()

    storage_helper.results.reset()
    temp_log_handler = storage_helper.logging.add_log_file_handler()

    logger = logging.getLogger(LOCAL_LOGGER_BASE_NAME + ".analyze_apk_file")
    logger.info(f'Starting analysis of "{apk_path}"')

    try_reinstall = False
    analysis_successful = False

    try:
        test_app = AndroidApp(apk_path)
    except FileNotFoundError:
        logger.error(f'The file "{apk_path}" could not be found!')
        storage_helper.logging.close_log_file_handler(
            handler=temp_log_handler,
            android_app=None,
            prefix=f"{apk_path.name}-",
        )
        return False

    if test_app:
        test_app.install_source = "APK"

    storage_helper.store_metadata_and_apk(
        test_app,
        move_apk=False,  # Don't store apk in output dir to avoid removing it from the input dir or duplicating it
    )

    test_exploration_session = None

    try:
        if android_device.emulator:
            if CREATE_PCAP:
                pcap_path = storage_helper.get_pcap_path(test_app)
            else:
                pcap_path = ""

            android_device.emulator.start(pcap_path=str(pcap_path), mitmproxy_address=MITMPROXY_ADDRESS)

        logger.debug(f"Creating exploration session")
        test_exploration_session = AndroidExplorationSession(
            command_executor=appium_server,
            device=android_device,
            app=test_app,
            install_app_from_apk=True,
        )
        test_exploration_session.start()
        logger.info(
            f'Exploring app "{(test_app.name if test_app.name else test_app.apk_path.name)}"'
        )
        print("Steps that are passed: {}".format(STEPS))
        test_exploration_session.explore(
            explorer=STRATEGY,
            benchmark=BENCHMARK,
            results_helper=storage_helper.results,
            steps=30,
        )
        logger.info(
            f'Finished exploring app "{(test_app.name if test_app.name else test_app.apk_path.name)}"'
        )
        analysis_successful = True

    except WebDriverException as err:
        logger.error(
            f'There was a "WebDriverException" while exploring the app '
            f'"{(test_app.name if test_app.name else test_app.apk_path.name)}"', exc_info=True
        )
        if (
            test_app.app_package
            and f"Cannot start the '{test_app.app_package}' application." in err.msg
            or "failed to install" in err.msg
        ):
            try_reinstall = True
            logger.info(
                "The previous error indicates that there was an issue while starting/installing the application."
            )
            logger.info("Trying to reinstall app from App Store and analyse again")
    finally:
        if test_exploration_session:
            logger.info("Stopping exploration session")
            test_exploration_session.stop()
        if android_device.emulator and android_device.emulator.is_running():
            android_device.emulator.stop()

    storage_helper.logging.close_log_file_handler(
        handler=temp_log_handler, android_app=test_app,
    )
    if try_reinstall:
        analysis_successful = analyze_app_package(
            appium_server=appium_server,
            android_device=android_device,
            package_name=test_app.app_package,
            storage_helper=storage_helper,
        )

    return analysis_successful






def analyze_many_apk_files(
    appium_server: "str",
    android_device: "AndroidDevice",
    apk_directory_path: "Path",
    storage_helper: "StorageHelper",
) -> None:
    """
    Installs and analyzes APK files in a given directory.

    Args:
        appium_server: Fully qualified address of the appium server
        android_device: Object holding all data of the test device
        apk_directory_path: Path pointing to a directory containing APK files
        storage_helper: Object to help store all necessary data
    """
    apk_paths = [
        apk_file for apk_file in apk_directory_path.glob("*.apk") if apk_file.is_file()
    ]
    seeded_random.reset_seeded_random()

    logger = logging.getLogger(LOCAL_LOGGER_BASE_NAME + ".analyze_many_apk_files")
    logger.info(f"Starting analysis of {len(apk_paths)} Apps.")

    count = 1
    for apk_path in apk_paths:
        print("Now processing number {}".format(count))
        count = count + 1

        try:
            s = str(apk_path)
            pcap_name = s[s.rfind("/") + 1: -4]
            #print("Pcap name : {}".format(pcap_name))
            # proc = multiprocessing.Process(target=dump_pcap, args=(s,))

            #print("Now starting tcpdump")
            # p = subprocess.Popen(["tcpdump", "-i", 'en0', "-w", 'new_traffic/{}.pcap'.format(pcap_name)], stdout=subprocess.PIPE)
            # p = subprocess.Popen(["tcpdump", "-i", 'en0', "port", "8080", "-w", 'new_traffic/{}.pcap'.format(pcap_name)],
            #                      stdout=subprocess.PIPE)

            subprocess.check_call(['/home/protick/Documents/2020-dynamic-analysis-android-master/code/mitm-ctr.sh', 'start', pcap_name])
            analyze_apk_file(appium_server, android_device, apk_path, storage_helper)
            subprocess.check_call(['/home/protick/Documents/2020-dynamic-analysis-android-master/code/mitm-ctr.sh', 'stop'])

            #print("Now terminating tcpdump")
            #p.terminate()

        except Exception as e:
            logger.exception(f"An unhandled exception occurred while analyzing {apk_path}: {e}")
