from ppadb.client import Client as AdbClient
import os
import subprocess
import time

apk_paths = ["/home/protick/net/data/cert-pinning/ios/common600/random_100"]

def get_files_from_path_v2(path, extension):
    retset = set()
    for f in os.listdir(path):
        #print(f)
        if f.endswith(extension):
            retset.add(path + "/" + f) # Need to track path as well
    return retset

apk_files = set()
for path in apk_paths:
    apk_files.update(get_files_from_path_v2(path, ".apk"))



client = AdbClient(host="127.0.0.1", port=5037)

devices = client.devices()
device = devices[0]


count = 0
for apk in apk_files:
    try:
        print("Analyzing {}".format(apk))
        last_index = apk.rfind("/") + 1
        package_name = apk[last_index: -4]

        device.install(apk)
        print("Installed {}".format(apk))
        #print("Now starting tcpdump")
        subprocess.check_call(['/home/protick/Documents/2020-dynamic-analysis-android-master/code/mitm-ctr.sh', 'start', package_name])
        device.shell("monkey -p {} -c android.intent.category.LAUNCHER 1".format(package_name))
        time.sleep(80)
        subprocess.check_call(['/home/protick/Documents/2020-dynamic-analysis-android-master/code/mitm-ctr.sh', 'stop'])
        device.uninstall(package_name)

        print("Uninstalled {}".format(apk))

    except Exception as e:
        print(e)
        try:
            if package_name:
                device.uninstall(package_name)
        except Exception as e:
            print(e)
            pass

        pass



