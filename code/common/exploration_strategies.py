import logging
from typing import TYPE_CHECKING

from helper.constants import LOGGER_BASE_NAME

if TYPE_CHECKING:
    from common.exploration_session import ExplorationSession

logger = logging.getLogger(LOGGER_BASE_NAME + ".exploration-strategies")


class ExplorationStrategy:
    """Handles app state analysis and decides on steps to take next."""

    def __init__(self, session: "ExplorationSession"):
        """Initializes with existing ExplorationSession."""
        self.session = session

    def explore(self, steps: int = 1000) -> None:
        """Starts automatic exploration.

        Args:
            steps: The number of steps to take to explore the app. Defaults to 1000.
        """
        logger.info("Starting exploration for {} steps".format(steps))
        for i in range(1, steps + 1):
            self.session.benchmark.before_step(step=i)
            self.execute_next_step()
            self.session.benchmark.after_step(step=i)

    def execute_next_step(self) -> None:
        raise NotImplementedError
