#!/usr/bin/env python3

from typing import TYPE_CHECKING

from appium import webdriver

if TYPE_CHECKING:
    from typing import Optional

    from common.exploration_benchmarks import ExplorationBenchmark


class ExplorationSession:
    """Holds all data necessary for exploration, manages Appium session."""
    benchmark: "ExplorationBenchmark"
    appium_wd: "Optional[webdriver.Remote]"
