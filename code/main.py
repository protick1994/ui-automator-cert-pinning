#!/usr/bin/env python3

import argparse
import logging
import sys
from pathlib import Path
from signal import SIGINT, signal
from typing import TYPE_CHECKING

import urllib3

import analyzer
from analyzer import analyze_apk_file, analyze_many_apk_files
from Android.device import AndroidDevice
from Android.emulator import AndroidEmulator
from helper import seeded_random
from helper.constants import LOG_FORMATTER, LOGGER_BASE_NAME
from helper.storage_helper import StorageHelper

if TYPE_CHECKING:
    from typing import Optional, Any
    from types import FrameType


def check_positive(value: "Any") -> "int":
    integer = int(value)
    if integer <= 0:
        raise argparse.ArgumentTypeError(f'{value} is an invalid positive int value')
    return integer


def check_path_exists(value: "Any") -> "Path":
    path = Path(value).absolute()
    if path.exists():
        return path
    raise argparse.ArgumentTypeError(f'{value} is not a path to an existing file or directory')


def main() -> None:
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--appium-server-address",
        default="localhost",
        metavar="ADDRESS",
        type=str,
        help="The IP-address/domain of the appium server",
    )
    parser.add_argument(
        "--appium-server-port",
        default=4723,
        metavar="PORT",
        type=int,
        help="The port of the appium server",
    )
    parser.add_argument(
        "--out-dir",
        type=lambda p: Path(p).absolute(),
        default=Path("./out_dir/").absolute(),
        help="The location of the directory the logs and results should be written to. "
        "If omitted, './out_dir/' is used.",
    )
    levels = ("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")
    parser.add_argument(
        "--verbosity",
        default="DEBUG",
        metavar="LEVEL",
        type=str,
        choices=levels,
        help="Specify the verbosity for console output.",
    )
    android_device_group = parser.add_mutually_exclusive_group(required=True)
    android_device_group.add_argument(
        "--adb-udid",
        metavar="UDID",
        type=str,
        help="UDID of the running (physical or virtual) device as it shows up, when issuing the command `adb devices`.",
    )
    android_device_group.add_argument(
        "--virtual-device-name",
        metavar="NAME",
        type=str,
        help="Name of the stopped Android Virtual Device (AVD) as displayed by `emulator -list-avds`."
        "The device will be backed-up and a fresh instance will be started for each app.",
    )
    android_device_group.add_argument(
        "--virtual-device-backup",
        metavar="PATH",
        type=check_path_exists,
        help="Path to a zip file containing a full backup of an AVD (.avd folder and .ini file). "
        "If the AVD already exists, it will be overwritten. A fresh instance will be started for each app.",
    )
    parser.add_argument(
        "--android-version",
        metavar="VERSION",
        type=str,
        help="Android version of the test device.",
        required=True,
    )
    analysis_options = parser.add_argument_group("Analysis Options", "Options to control analysis parameters")
    analysis_options.add_argument(
        "--steps",
        metavar="STEPS",
        type=check_positive,
        default=25,
        help="Set number of steps each analysis takes. Must be a positive integer."
    )
    analysis_options.add_argument(
        "--capture-network-traffic",
        action='store_true',
        default=False,
        help="Capture all network traffic generated during analysis. NOTE: This only works with emulators as of now. "
             "For all other setups, you have to do so by yourself."
    )
    analysis_options.add_argument(
        "--mitmproxy-address",
        metavar="ADDRESS",
        type=str,
        default="",
        help="Route all http(s) traffic through a mitmproxy server running at ADDRESS. By default mitmproxy listens at "
             "0.0.0.0:8080 if started. NOTE: This only works with emulators as of now."
    )
    app_stores = ("GOOGLE_PLAY", "NONE")
    analysis_options.add_argument(
        "--allow-reinstall-from",
        metavar="APPSTORE",
        type=str,
        default="NONE",
        choices=app_stores,
        help="Allow to reinstall for apps that cannot be installed from APK. \"NONE\" means that reinstall is "
             "disallowed. NOTE: The device has to be prepared for usage of this appstore."
    )
    analysis_options.add_argument(
        "--random-seed",
        metavar="SEED",
        type=str,
        default="",
        help="Set the seed for all randomness to SEED, if not set, it is randomly generated."
    )
    parser.add_argument(
        "apk_path",
        metavar="PATH",
        type=check_path_exists,
        help="Path to either a directory with APK files or a single APK file to be tested.",
    )

    args = parser.parse_args()

    verbosity = args.verbosity
    apk_path = args.apk_path

    appium = f"http://{args.appium_server_address}:{args.appium_server_port}/wd/hub"

    # Create new and unique output-folder
    run_index = 0
    while (args.out_dir / f"run-{run_index:04d}").exists():
        run_index += 1
    output_directory: "Path" = args.out_dir / f"run-{run_index:04d}"
    output_directory.mkdir(parents=True, exist_ok=False)

    logger = logging.getLogger(LOGGER_BASE_NAME)
    logger.setLevel(logging.DEBUG)

    # Create log handler for console output
    log_stream_handler = logging.StreamHandler(stream=sys.stdout)
    log_stream_handler.setFormatter(LOG_FORMATTER)
    log_stream_handler.setLevel(verbosity)
    logger.addHandler(log_stream_handler)

    logger.debug(f"Logging everything to: {output_directory / 'global_debug.log'}")
    global_debug_log_file_handler = logging.FileHandler(
        output_directory / "global_debug.log", mode="a"
    )
    global_debug_log_file_handler.setFormatter(LOG_FORMATTER)
    global_debug_log_file_handler.setLevel(logging.DEBUG)
    logger.addHandler(global_debug_log_file_handler)

    if args.random_seed:
        seeded_random.SEED = args.random_seed
    logger.info(f'Randomness is seeded with seed "{seeded_random.SEED}"')

    try:
        appium_status_code = (
            urllib3.PoolManager().request("GET", appium + "/sessions/").status
        )
        if appium_status_code != 200:
            logger.critical(
                f"Appium doesn't seem to be running.{appium}/sessions/"
                f" returned HTTP status code {appium_status_code}"
            )
            exit(appium_status_code)
    except urllib3.exceptions.MaxRetryError:
        logger.critical(
            f"Appium is not available, ensure that there is a running instance reachable at "
            f"{args.appium_server_address}:{args.appium_server_port}"
        )
        exit(1)

    android_device = AndroidDevice(
        backup_path=args.virtual_device_backup,
        avd_name=(args.virtual_device_name if args.virtual_device_name else ""),
        name=args.adb_udid,
        udid=args.adb_udid,
        platform_version=args.android_version,
    )

    def cleanup(sig: "int" = -1, _: "Optional[FrameType]" = None) -> None:
        """Function used as signal-handler and for normal cleanup after the analysis is finished or crashed"""
        # A signal handler has to have two parameters
        if sig == SIGINT:
            logger.info("SIGINT received - cleaning up and shutting down")
        else:
            logger.info("Cleaning up")
        if android_device.emulator:
            android_device.emulator.delete()
        # Attempt to shut down logging trying to avoid a crash
        logging.shutdown()
        if sig == SIGINT:
            exit(1)

    signal(SIGINT, cleanup)
    try:
        if not android_device.name:
            logger.info("Preparing emulator for analysis")
            android_device.emulator = AndroidEmulator(
                avd_name=android_device.avd_name, backup_path=android_device.backup_path
            )
            android_device.name = android_device.emulator.emulator_name
            android_device.udid = android_device.emulator.emulator_name
        logger.info(f"Using device with udid {android_device.udid} for analysis")
        # Create StorageHelper object to enable storing of analysis results
        storage_helper = StorageHelper(
            output_dir=output_directory,
            base_logger=logger,
            log_level=verbosity,
            log_formatter=LOG_FORMATTER,
        )

        analyzer.STEPS = args.steps
        analyzer.CREATE_PCAP = args.capture_network_traffic
        analyzer.MITMPROXY_ADDRESS = args.mitmproxy_address
        analyzer.ALLOW_REINSTALL_FROM = args.allow_reinstall_from

        if apk_path.is_file():
            logger.info("Starting analysis of single apk")
            analyze_apk_file(appium, android_device, apk_path, storage_helper)
        elif apk_path.is_dir():
            logger.info(f"Starting analysis of all APKs in {apk_path}")
            analyze_many_apk_files(appium, android_device, apk_path, storage_helper)

        cleanup()
    except Exception as error:
        # If there is any unhandled exception, delete emulator and reraise exception
        try:
            if android_device.emulator:
                cleanup()
        except Exception as error2:
            raise error2 from error
        raise error


if __name__ == "__main__":
    main()
