from dataclasses import dataclass
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Optional


@dataclass
class iOSDevice:
    """Encapsulates information about a iOS-based test device."""

    udid: "str"
    name: "str" = "iOS Device"


@dataclass
class App:
    """Encapsulates information about an app."""

    path: "Optional[str]" = None
    name: "Optional[str]" = None  # Display-name of the app


@dataclass
class iOSApp(App):
    """Encapsulates information about a iOS-app."""

    bundle_id: "Optional[str]" = None
