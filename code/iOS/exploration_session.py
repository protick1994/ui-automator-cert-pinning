import logging
from typing import TYPE_CHECKING

from appium import webdriver
from urllib3.exceptions import MaxRetryError

from common.exploration_session import ExplorationSession
from iOS.datastructure import iOSApp, iOSDevice

if TYPE_CHECKING:
    from common.exploration_benchmarks import ExplorationBenchmark
    from common.exploration_strategies import ExplorationStrategy


class iOSExplorationSession(ExplorationSession):
    """Handles Appium-based test sessions."""

    def __init__(self, command_executor: str, device: iOSDevice, app: iOSApp):
        """Prepare a test session.

        Keyword arguments:
        command_executor -- URL of the WDA server to use as a mediator.
        device -- information about the device on which tests are executed.
        app -- the app to run tests on.
        """
        self.command_executor = command_executor
        self.device = device
        self.app = app
        self.appium_wd = None
        self.appium_wd_app = None
        self.explorer = None
        self.benchmark = None
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)
        logger_handler = logging.StreamHandler()
        logger_handler.setFormatter(
            logging.Formatter("%(asctime)s: (%(levelname)s) %(name)s: %(message)s")
        )
        self.logger.addHandler(logger_handler)

    def __del__(self):
        if self.appium_wd:
            self.logger.info("Quitting Appium session")
            self.appium_wd.quit()

    def start(self):
        """Start an Appium-session using the device and app provided during initialization."""
        if not self.appium_wd:
            desired_caps = {
                "platformName": "iOS",
                "automationName": "XCUITest",
                "deviceName": self.device.name,
                "udid": self.device.udid,
                "bundleId": self.app.bundle_id,
                "useJSONSource": "true",
                "newCommandTimeout": 600,
            }
            self.logger.info("Starting session")

            try:
                self.appium_wd = webdriver.Remote(self.command_executor, desired_caps)
            except MaxRetryError:
                self.logger.error("Could not connect to WDA command executor")
                return

            self.appium_wd_app = self.appium_wd.find_element_by_class_name(
                "XCUIElementTypeApplication"
            )

    def stop(self):
        """Quits a running Appium session."""
        if self.appium_wd:
            self.logger.info("Quitting Appium session")
            self.appium_wd.quit()
            self.appium_wd = None
            self.appium_wd_app = None

    def explore(
        self,
        explorer: "type(ExplorationStrategy)",
        benchmark: "type(ExplorationBenchmark)",
        steps: int = 1000,
    ):
        """Explore the app automatically.

        Keyword arguments:
        explorer -- ExplorationStrategy handling actual app analysis and control.
        benchmark -- ExplorationBenchmark analysing and measuring explorer behaviour.
        steps -- number of steps for which app should be explored. Defaults to 1000.
        """
        if not self.appium_wd:
            self.logger.info("No Appium session found, starting new one.")
            self.start()
        self.explorer = explorer(self)
        self.benchmark = benchmark(self)
        self.explorer.explore(steps=steps)
        self.explorer = None
        self.benchmark = None
