import random
from queue import LifoQueue
from typing import TYPE_CHECKING

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.wait import WebDriverWait

from common.exploration_strategies import ExplorationStrategy

if TYPE_CHECKING:
    from common.exploration_session import ExplorationSession
    from iOS.exploration_session import iOSExplorationSession


class RandomButtonExplorationStrategy(ExplorationStrategy):
    """Explores an app by randomly tapping visible buttons."""

    def execute_next_step(self):
        """Searches for visible buttons and selects one to tap at random."""

        self.logger.info("Executing next step")
        visible_buttons = self.session.appium_wd_app.find_elements_by_ios_predicate(
            "type == 'XCUIElementTypeButton' AND visible == 1"
        )
        if len(visible_buttons) == 0:
            self.logger.info("No visible buttons found")
        else:
            selected_button = random.choice(visible_buttons)
            self.logger.info("Pressing visible button {}".format(selected_button))
            selected_button.click()


class NonRepeatingRandomButtonExplorationStrategy(ExplorationStrategy):
    """Explores an app by randomly tapping visible buttons at most once per run."""

    def __init__(self, session: "ExplorationSession"):
        super().__init__(session)
        self.tapped_buttons = []

    def execute_next_step(self):
        """Searches for visible buttons and taps one that hasn't been selected before at random."""
        visible_buttons = self.session.appium_wd_app.find_elements_by_ios_predicate(
            "type in {'XCUIElementTypeButton', 'XCUIElementTypeToolbarButton'} AND visible == 1"
        )
        visible_buttons_untapped = list(
            filter(lambda b: b not in self.tapped_buttons, visible_buttons)
        )
        if len(visible_buttons) == 0:
            self.logger.info("No visible buttons found, doing nothing")
            return
        elif len(visible_buttons_untapped) == 0:
            self.logger.info(
                "No visible untapped buttons found, selecting from tapped ones"
            )
            visible_buttons_untapped = visible_buttons

        self.logger.info(
            "Selecting from {} visible untapped buttons".format(
                len(visible_buttons_untapped)
            )
        )
        selected_button = random.choice(visible_buttons_untapped)
        self.tapped_buttons.append(selected_button)
        self.logger.info("Pressing visible button {}".format(selected_button))
        selected_button.click()


class iOSExplorationStrategy(ExplorationStrategy):
    """Handles iOS-specific analysis."""

    class XCUIElement:
        def __init__(self, properties_dict):
            for key in properties_dict:
                if key != "children":
                    setattr(self, key, properties_dict[key])

        def __eq__(self, other: "object") -> "bool":
            comparison_values = ["type", "rect", "name", "label"]
            for attribute_name in comparison_values:
                if getattr(self, attribute_name, None) != getattr(
                    other, attribute_name, None
                ):
                    return False
            return True

        def __lt__(self, other):
            return (self.depth > other.depth) and (
                self.rect("y"),
                self.rect("x") < other.rect("y"),
                other.rect("x"),
            )

        def __hash__(self) -> "int":
            return hash(
                (
                    frozenset(self.rect),
                    self.label if self.label else "",
                    self.name if self.name else "",
                    self.value if self.value else "",
                )
            )

    # https://developer.apple.com/documentation/xctest/xcuielementtype
    WIDGET_BUTTON_CLASSES = [
        "Button",
        "Cell",
        "Link",
        "Menu",  # ?
        "MenuBarItem",
        "MenuButton",
        "MenuItem",
        "TabBar",
        "ToolbarButton",
    ]
    WIDGET_INPUT_CLASSES = [
        "TextField",
        "CheckBox",
        "Picker",
        "PickerWheel",
        "RadioButton",  # RadioGroup?
        "SearchField," "SecureTextField",
        "SegmentedControl",
        "Slider",
        "Stepper",
        "Switch",
        "TextField",
        "Toggle",
    ]
    WIDGET_SKIP_CLASSES = [
        "Keyboard",
        "StatusBar",
    ]
    LOGIN_GOOGLE_USER = "pebaujb01@gmail.com"

    tapped_buttons = []

    def __init__(self, session: "iOSExplorationSession"):
        super(iOSExplorationStrategy, self).__init__(session)

    def score_button_widgets(self, button_widgets, graph):
        selected_button = None
        for bt in button_widgets:
            bt_label = getattr(bt, "label", "")
            bt.score = 1000
            bt.score *= bt.depth / 10
            bt.score *= int(
                bt.isVisible
            )  # TODO: Different rating for buttons in ScrollView
            bt.score *= int(
                bt.isEnabled
            )  # TODO: Rank disabled buttons higher trying to enable them by filling input widgets
            bt.score += 500 * (1 if bt_label and "log in" in bt_label.lower() else 0)
            bt.score -= 500 * (1 if bt_label and "dictate" in bt_label.lower() else 0)
            tap_count = len([t for t in self.tapped_buttons if bt.__eq__(t)])
            bt.score /= (tap_count + 1) if tap_count > 0 else 1
            # TODO: Rank MenubarButtons/TabBars lower
            # TODO: Rank keyboard buttons lower/disable them

            if not selected_button or bt.score > selected_button.score:
                selected_button = bt

        if not selected_button:
            self.logger.info("No buttons found")
            return

        return button_widgets, selected_button

    def execute_next_step(self):
        """Analyse current screen and generate appropriate inputs.

        A single step may consist of multiple on-screen actions, e.g. text input and taps.
        """
        self.session.appium_wd.execute_script(
            "mobile: activateApp", {"bundleId": self.session.app.bundle_id}
        )
        (graph, widgets_input, widgets_buttons) = self.parse_json_source()

        (scored_button_widgets, selected_button) = self.score_button_widgets(
            widgets_buttons, graph
        )
        # Determine input widget order
        widgets_input_sorted = sorted(widgets_input)

        # Tap selected button
        if not selected_button:
            return
        self.tapped_buttons.append(selected_button)
        try:
            self.logger.debug(
                "Selected {} button {} with score {}, hash {} to tap: {}".format(
                    selected_button.type,
                    selected_button.label,
                    selected_button.score,
                    hash(selected_button),
                    selected_button.__dict__,
                )
            )
            self.session.appium_wd.find_element_by_ios_predicate(
                """type == 'XCUIElementType{type}' 
                AND rect.x == {x} AND rect.y == {y} 
                AND rect.width == {width} AND rect.height == {height}""".format(
                    type=selected_button.type,
                    x=selected_button.rect["x"],
                    y=selected_button.rect["y"],
                    width=selected_button.rect["width"],
                    height=selected_button.rect["height"],
                )
            ).click()

            try:
                bt_label = getattr(selected_button, "label", "")
                if bt_label and " with google" in bt_label.lower():
                    WebDriverWait(self.session.appium_wd, 10, 1).until(
                        lambda x: x.find_element_by_ios_predicate(
                            "type == 'XCUIElementTypeAlert'"
                        )
                    )
                    self.session.appium_wd.execute_script(
                        "mobile: alert", {"action": "accept"}
                    )
                    self.login_with_google()
            except AttributeError as e:
                self.logger.error(
                    "AttributeError occured when looking for Google button: {}".format(
                        str(e)
                    )
                )
        except NoSuchElementException as e:
            self.logger.error(
                "Could not find selected button to tap: {}".format(str(e))
            )

    def parse_json_source(self):
        """Fetch JSON representation of current app state and create view hierarchy graph,
        input and button widget lists.
        """
        json_source = self.session.appium_wd.execute_script(
            "mobile:source", {"format": "json"}
        )

        graph = {}
        widgets_input = []
        widgets_buttons = []
        queue = LifoQueue()

        json_source["depth"] = 0
        queue.put((json_source, None))

        while not queue.empty():
            (element, parent) = queue.get()
            converted_element = self.XCUIElement(element)
            if converted_element.type in self.WIDGET_SKIP_CLASSES:
                self.logger.info(
                    "Ignoring element with type: {}".format(converted_element.type)
                )
                continue
            graph[converted_element] = []
            if parent:
                graph[converted_element] = [parent]
                graph[parent].append(converted_element)
            if converted_element.type in self.WIDGET_BUTTON_CLASSES:
                widgets_buttons.append(converted_element)
            elif converted_element.type in self.WIDGET_INPUT_CLASSES:
                widgets_input.append(converted_element)
            if "children" in element:
                for child in element["children"]:
                    child["depth"] = converted_element.depth + 1
                    queue.put((child, converted_element))

        return (graph, widgets_input, widgets_buttons)

    def login_with_google(self):
        """Handles SSO login flow using Google accounts.
        TODO: Also handle initial login. Right now this requires manual login once.
        """
        self.logger.info("Logging in with Google Account")
        if not self.LOGIN_GOOGLE_USER:
            self.logger.error("Google user name not set")
            # TODO: Dismiss web view
            return
        try:
            account_selection_link = WebDriverWait(self.session.appium_wd, 10, 1).until(
                lambda x: x.find_element_by_ios_predicate(
                    "type == 'XCUIElementTypeLink' AND name LIKE[c] '{}*'".format(
                        self.LOGIN_GOOGLE_USER
                    )
                )
            )
            self.logger.info(
                "Successfully waited for account selection link: {}".format(
                    account_selection_link
                )
            )
        except TimeoutException:
            self.logger.info("Did not find account selection link in time")
            # TODO: Dismiss web view
            return
        account_selection_link.click()
        WebDriverWait(self.session.appium_wd, 10).until_not(
            lambda x: x.find_element_by_ios_predicate(
                "type == 'XCUIElementTypeLink' AND name LIKE [c] '{}*'".format(
                    self.LOGIN_GOOGLE_USER
                )
            )
        )
        self.logger.info("Google login flow ended")


class iOSDFSExplorationStrategy(iOSExplorationStrategy):
    """Depth-first search exploring views in detail before switching to different ones."""

    def score_button_widgets(self, button_widgets, graph):
        selected_button = None
        for bt in button_widgets:
            if bt.isVisible == "0" or bt.isEnabled == "0":
                bt.score = 0
            elif bt.label and "google" in bt.label:
                bt.score = 100
            else:
                bt.score = bt.depth
                tap_count = len([t for t in self.tapped_buttons if bt.__eq__(t)])
                if tap_count:
                    bt.score /= tap_count * 2
            if not selected_button or bt.score > selected_button.score:
                selected_button = bt

        if not selected_button:
            self.logger.info("No buttons found")
            return (None, None)

        return button_widgets, selected_button


class iOSBFSExplorationStrategy(iOSExplorationStrategy):
    """Breadth-first search focusing on visiting different views."""

    def score_button_widgets(self, button_widgets, graph):
        selected_button = None
        for bt in button_widgets:
            if bt.isVisible == "0" or bt.isEnabled == "0":
                bt.score = 0
            else:
                bt.score = 100 * (1 / bt.depth)
                tap_count = len([t for t in self.tapped_buttons if bt.__eq__(t)])
                if tap_count:
                    bt.score /= tap_count * 2
            if not selected_button or bt.score > selected_button.score:
                selected_button = bt

        if not selected_button:
            self.logger.info("No buttons found")
            return (None, None)

        return button_widgets, selected_button


class iOSRandomButtonExplorationStrategy(iOSExplorationStrategy):
    """Selecting visible, enabled buttons at random."""

    def score_button_widgets(self, button_widgets, graph):
        selected_button = None
        for bt in button_widgets:
            if bt.isVisible == "1" and bt.isEnabled == "1":
                bt.score = random.randint(0, 100)
            else:
                bt.score = 0
            if not selected_button or bt.score > selected_button.score:
                selected_button = bt

        if not selected_button:
            self.logger.info("No buttons found")
            return (None, None)

        return button_widgets, selected_button
